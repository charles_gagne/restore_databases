### README

## What's it for?
This script has been created to restore DB's on a SQL instance.  It reads the target volume and looks for any backup files contained (recursively) and attempts to create the DBs and restore.
Presently it only restores to the default SQL locations (C: Drive).

## Prerequisistes
 
* Install Windows Management Framework 5.1 - https://www.microsoft.com/en-us/download/details.aspx?id=54616
* Install DBATools - https://dbatools.io/download/

## How do I use it?
After installing pre--requisites the command can be run as follows.

PS> .\Restore_Databases.ps1 <Path where backups are located>

Example: .\Restore_Databases.ps1 D:\full