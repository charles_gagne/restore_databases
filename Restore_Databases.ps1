Param   (
            [string] $BackupPath
        )


$backupRoot = Get-ChildItem -Path $BackupPath 
$server = "." 
$dbToMiss = "model","msdb","master" # Databases to miss during recovery

    # For each folder in the backup root directory
foreach($folder in $backupRoot)
{   
    # Get the most recent .bak files for all databases
    $backupFiles = Get-ChildItem -Path $folder.FullName -Filter "*.bak" -Recurse | Sort-Object -Property CreationTime -Descending | Select-Object -First 1
    
    # For each .bak file...
    foreach ($backupFile in $backupFiles)
    {
        # Get the database Name from the backup
        $databaseName =  $(Read-DbaBackupHeader -server . -path $backupFile.FullName).DatabaseName
        # If not in list of DB's to miss restore DB
        if(!$dbToMiss.Contains($databaseName))
        {
            Write-Host ("Restoring Backup: $databasename")
            Restore-DbaDatabase -SqlInstance . -Path $backupFile.FullName -WithReplace
        }
    }
} 